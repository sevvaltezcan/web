﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(b141210046.Startup))]
namespace b141210046
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
