﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210046.Models;
using System.IO;

namespace WebUI.Models.Data
{
    public class slidersController : Controller
    {
        private sliderEntities1 db = new sliderEntities1();

        // GET: sliders
        public ActionResult Index()
        {
            return View(db.sliders.ToList());
        }

        public ActionResult Slider()
        {
            using (sliderEntities1 context = new sliderEntities1())
            {
                var slider = context.sliders.ToList();
                return View(slider);
            }
        }

        public ActionResult SlideEkle()
        {
            return View();
        }

        public ActionResult SlideDuzenle(int SlideID)
        {
            using (sliderEntities1 context = new sliderEntities1())
            {
                var _slideDuzenle = context.sliders.Where(x => x.ID == SlideID).FirstOrDefault();
                return View(_slideDuzenle);
            }
        }

        public ActionResult SlideSil(int SlideID)
        {
            try
            {
                using (sliderEntities1 context = new sliderEntities1())
                {
                    context.sliders.Remove(context.sliders.First(d => d.ID == SlideID));
                    context.SaveChanges();
                    return RedirectToAction("Slider", "sliders");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

        [HttpPost]
        public ActionResult SlideEkle(sliders s, HttpPostedFileBase file)
        {
            try
            {
                using (sliderEntities1 context = new sliderEntities1())
                {
                    sliders _slide = new sliders();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slide.SliderFoto = memoryStream.ToArray();
                    }
                    _slide.SliderText = s.SliderText;
                    _slide.BaslangicTarih = s.BaslangicTarih;
                    _slide.BitisTarih = s.BitisTarih;
                    context.sliders.Add(_slide);
                    context.SaveChanges();
                    return RedirectToAction("Slider", "sliders");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        [HttpPost]
        public ActionResult SlideDuzenle(sliders slide, HttpPostedFileBase file)
        {
            try
            {
                using (sliderEntities1 context = new sliderEntities1())
                {
                    var _slideDuzenle = context.sliders.Where(x => x.ID == slide.ID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slideDuzenle.SliderFoto = memoryStream.ToArray();
                    }
                    _slideDuzenle.SliderText = slide.SliderText;
                    _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                    _slideDuzenle.BitisTarih = slide.BitisTarih;
                    context.SaveChanges();
                    return RedirectToAction("Slider", "sliders");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }
    }
}
