﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210046.Models;
using System.Threading;
using System.Globalization;

namespace b141210046.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hakkımızda";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "İletişim";

            return View();
        }

        public ActionResult Galeri()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Spor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Spor(spor spor)
        {
            return View();
        }

        public ActionResult Change(string lang)
        {
            if (lang != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
            }
            HttpCookie cookie = new HttpCookie("_lang");
            cookie.Value = lang;
            Response.Cookies.Add(cookie);
            return View("Index");
        }
    }
}